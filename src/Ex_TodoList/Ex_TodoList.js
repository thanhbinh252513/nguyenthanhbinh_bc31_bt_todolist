import React, { Component } from "react";

import { ThemeProvider } from "styled-components";
import { Button } from "./ComponentsTodoList/Button";
import { Container } from "./ComponentsTodoList/Container";
import { Dropdown } from "./ComponentsTodoList/Dropdown";
import {
  Heading1,
  Heading2,
  Heading3,
  Heading4,
  Heading5,
} from "./ComponentsTodoList/Heading";
import { Table, Thead, Tr, Tbody, Td, Th } from "./ComponentsTodoList/Table";
import { Label, TextField, Input } from "./ComponentsTodoList/TextField";

import { DarkThemeTodoList } from "./Themes/DarkThemeTodoList";
import { LightThemeTodoList } from "./Themes/LightThemeTodoList";
import { connect } from "react-redux";
import {
  addTaskAction,
  deleteTaskActione,
  doneTaskActione,
  editTaskActione,
} from "./redux/actions/ToDoListActions";
import { arrTheme } from "./Themes/ThemeManager";

class Ex_TodoList extends Component {
  state = {
    taskName: "",
  };

  renderTaskToDo = () => {
    return this.props.taskList
      .filter((task) => !task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.props.dispatch(editTaskActione(task));
                }}
                className="ml-1"
              >
                <i className="fa fa-edit"></i>
              </Button>

              <Button
                onClick={() => {
                  this.props.dispatch(doneTaskActione(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-check"></i>
              </Button>

              <Button
                onClick={() => {
                  this.props.dispatch(deleteTaskActione(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };
  renderTaskCompleted = () => {
    return this.props.taskList
      .filter((task) => task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.props.dispatch(deleteTaskActione(task.id));
                }}
                className=""
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };
  // handleChange =(e)=>{
  //   let (name,value)=e.target.value;
  //   this.setState({
  //     [name]:value
  //   })
  // }
  renderTheme = () => {
    return arrTheme.map((theme, index) => {
      return <option value={theme.id}>{theme.name}</option>;
    });
  };

  render() {
    return (
      <ThemeProvider theme={this.props.themeToDoList}>
        <Container className="w-50">
          <Dropdown
            onChange={(e) => {
              let { value } = e.target;
              this.props.dispatch({
                type: "change_theme",
                themeId: value,
              });
            }}
          >
            {this.renderTheme()}
          </Dropdown>
          <Heading3>To do list</Heading3>
          <TextField
            value={this.props.taskEdit.taskName}
            onClick={(e) => {
              this.setState(
                {
                  taskName: e.target.value,
                },
                () => {
                  console.log(this.state);
                }
              );
            }}
            name="taskName"
            label="task name"
            className="w-50"
          />
          <Button
            onClick={() => {
              let { taskName } = this.state;
              let newTask = {
                id: Date.now(),
                taskName: taskName,
                done: false,
              };

              this.props.dispatch(addTaskAction(newTask));
            }}
            className="ml-2"
          >
            <i className="fa fa-plus"></i>Add task
          </Button>
          <Button className="ml-2">
            <i className="fa fa-upload"></i>Update task
          </Button>

          <Heading3>Task to do</Heading3>
          <Table>
            <Thead>{this.renderTaskToDo}</Thead>
          </Table>
          <Heading3>Task completed</Heading3>
          <Table>
            <Thead>{this.renderTaskCompleted}</Thead>
          </Table>
        </Container>
      </ThemeProvider>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    themeToDoList: state.ToDoListReducer.themeToDoList,
    taskList: state.ToDoListReducer.taskList,
    taskEdit: state.ToDoListReducer.taskEdit,
  };
};
export default connect(mapStateToProps)(Ex_TodoList);
