import { DarkThemeTodoList } from "../../Themes/DarkThemeTodoList";
import { arrTheme } from "../../Themes/ThemeManager";
import {
  add_task,
  change_theme,
  delete_task,
  done_task,
  edit_task,
} from "../types/ToDoListTypes";

const initialState = {
  themeToDoList: DarkThemeTodoList,
  taskList: [
    { id: "task-1 ", taskName: "task 1 ", done: true },
    { id: "task-2 ", taskName: "task 2 ", done: false },
    { id: "task-3 ", taskName: "task 3 ", done: true },
    { id: "task-4 ", taskName: "task 4 ", done: false },
  ],
  taskEdit: { id: "task-1 ", taskName: "task 1 ", done: false },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case add_task: {
      // console.log('todo',action.newtask);
      // kiem tra trong
      if (action.taskName.name.trim() === "") {
        alert("Task name is required");
        return { ...state };
      }
      //kiemtra ton tai
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex(
        (task) => task.taskName === action.newTask.taskName
      );
      if (index !== 1) {
        alert("task name alrealy exists !");
        return { ...state };
      }
      taskListUpdate.push(action.newTask);
      state.taskList = taskListUpdate;
      return { ...state };
    }
    case change_theme: {
      let theme = arrTheme.find((theme) => theme.id == action.themeId);
      if (theme) {
        state.themeToDoList = { ...theme.theme };
      }
      return { ...state };
    }
    case done_task: {
      // click vao button check =>dispatch len action co task id
      let taskListUpdate = [...state.taskList];
      // tu task id tim ra task do
      let index = taskListUpdate.findIndex((task) => task.id === action.taskId);
      if (index !== -1) {
        taskListUpdate[index].done = true;
      }

      return { ...state, taskList: taskListUpdate };
    }
    case delete_task: {
      let taskListUpdate = [...state.taskList];
      // lay ra gia tri mang tasklistup = chinh no nhung filter k co taskid do
      taskListUpdate = taskListUpdate.filter(
        (task) => task.id !== action.taskId
      );
      return { ...state, taskList: taskListUpdate };
      // return {...state, taskList: state.taskList.filter(task=>task.id !==action.taskId)}
    }
    case edit_task: {
      return { ...state, taskEdit: action.task };
    }
    default:
      return { ...state };
  }
};
