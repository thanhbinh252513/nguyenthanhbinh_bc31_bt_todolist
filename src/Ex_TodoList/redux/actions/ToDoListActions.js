import {
  add_task,
  change_theme,
  delete_task,
  done_task,
  edit_task,
} from "../types/ToDoListTypes";

export const addTaskAction = (newTask) => {
  return {
    type: add_task,
    newTask,
  };
};
export const changeThemeAction = (themeId) => {
  return {
    type: change_theme,
    themeId,
  };
};

export const doneTaskActione = (taskId) => ({
  type: done_task,
  taskId,
});
export const deleteTaskActione = (taskId) => ({
  type: delete_task,
  taskId,
});
export const editTaskActione = (task) => ({
  type: edit_task,
  task,
});
